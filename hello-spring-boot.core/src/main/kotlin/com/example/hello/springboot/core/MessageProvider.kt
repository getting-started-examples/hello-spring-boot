package com.example.hello.springboot.core

interface MessageProvider {
    fun provideMessage() : String
}