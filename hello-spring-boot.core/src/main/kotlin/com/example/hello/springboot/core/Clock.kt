package com.example.hello.springboot.core

import java.time.ZonedDateTime

interface Clock {
    val now: ZonedDateTime
}
