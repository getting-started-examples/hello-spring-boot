package com.example.hello.springboot.fake

import com.example.hello.springboot.core.MessageProvider

class HardcodedMessageProvider : MessageProvider {
    override fun provideMessage(): String {
        return "Hello, (fake) World!"
    }
}