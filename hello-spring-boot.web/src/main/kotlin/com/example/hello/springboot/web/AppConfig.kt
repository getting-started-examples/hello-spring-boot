package com.example.hello.springboot.web

import com.example.hello.springboot.core.Clock
import com.example.hello.springboot.core.MessageProvider
import com.example.hello.springboot.fake.HardcodedMessageProvider
import com.example.hello.springboot.system.SystemClock
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class AppConfig {
    @Bean
    open fun messageProvider() : MessageProvider {
        return HardcodedMessageProvider()
    }

    @Bean
    open fun clock() : Clock {
        return SystemClock()
    }
}