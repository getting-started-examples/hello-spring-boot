package com.example.hello.springboot.web

import com.example.hello.springboot.core.Clock
import com.example.hello.springboot.core.MessageProvider
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController;
import java.time.ZonedDateTime

@RestController
@RequestMapping("hello")
class HelloController(private val clock : Clock, private val messageProvider: MessageProvider) {
    @GetMapping("/v1")
    fun hello() : HelloResponse {
        return HelloResponse(messageProvider.provideMessage(), clock.now);
    }
}

@RestController
@RequestMapping("hello")
class HelloController2(private val clock : Clock, private val messageProvider: MessageProvider) {
    @GetMapping("/v2")
    fun hello() : HelloResponse {
        return HelloResponse(messageProvider.provideMessage() + " [2]", clock.now);
    }
}

data class HelloResponse(val text: String, val now: ZonedDateTime)