package com.example.hello.springboot.system

import com.example.hello.springboot.core.Clock
import java.time.ZonedDateTime

class SystemClock : Clock {
    override val now: ZonedDateTime
        get() = ZonedDateTime.now()
}